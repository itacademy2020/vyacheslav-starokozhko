<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <link rel="stylesheet" href="style.css">
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <div class="header">


    <div class="logo" id="navlogo">
      <h1>Web.Calculate</h1>
    </div>

    <div class="seach" id="Top">
  <a href="#" class="icon" id="ic">&#8801;</a>
  <a href="#">Contacts</a>
  <a href="#">Information</a>
  <a href="#">Modules</a>
  <a href="#" id="menu">Calculeted</a>
  <a href="#" >Home</a>


    </div>
    </div>

    <div id="myclass" class="class">
        <a href="#" id="one">Вирахування потужності електродвигуна</a>
          <a href="#">$</a>
            <a href="#">&#8801;</a>
              <a href="#">&#8801;</a>
                <a href="#">&#8801;</a>
    </div>


    <div class="for" id="formula_one">
      <h1 align="center">Вирахування потужності електродвигуна</h1>
      <a href="http://server.com/Calculate_Project/index_react/Step%200.php">КРОК 0. Визначення подачі свердління у залежності від діаметра ріжучого інструменту</a>
      <a href="http://server.com/Calculate_Project/index_react/Step%201.php">КРОК 1. Вираховування поправочного коефіцієнта на осьову складову сили різання, Kp</a>
      <a href="#" id="two">КРОК 2. Вираховування осьової складової сили різання, Р0, Н</a>
      <a href="#" id="three">КРОК 3. Вираховування поправочного коефіцієнта, Kv, який враховує вплив механічних властивостей оброблювального матеріалу на швидкість різання, де оброблювальний матеріал сталь конструкційна вуглеродиста, легована і стальне лиття при матеріалі ріжучої частини інструмента - швидкоріжуча сталь</a>
      <a href="http://server.com/Calculate_Project/index_react/Step%204.php">КРОК 4. Вираховування швидкості головного руху різання, що допускається ріжучими властивостями свердла v, мм/хв</a>
      <a href="http://server.com/Calculate_Project/index_react/Step%205.php">КРОК 5. Вираховування частоти обертання шпинделя, що відповідає знайденій швидкості головного руху різання n, хв</a>
      <a href="http://server.com/Calculate_Project/index_react/Step%206.php">КРОК 6. Вираховування дійсної швидкості головного руху різання vд, м/хв</a>
      <a href="http://server.com/Calculate_Project/index_react/Step%207.php">КРОК 7. Вираховування крутного моменту від сил опору різання при свердлінні М, Нм</a>
      <a href="http://server.com/Calculate_Project/index_react/Step%208.php">КРОК 8. Вираховування потужності, яка витрачається на різання N, кВт</a>
      <a href="http://server.com/Calculate_Project/index_react/Step%209.php">КРОК 9. Перевірка достатності дійсної потужності двигуна головного приводу верстата, яка витрачається на різання N, кВт. Якщо N різання (Крок 8) < N шпинделя (Крок 9), то обробка деталі вказаної у завданні можлива. Інакше, обрати менше значення частоти обертання шпинделя у Кроці 6.</a>
      <a href="http://server.com/Calculate_Project/index_react/Step%2010.php">КРОК 10. Вираховування потужності електродвигуна приводу подачі свердлильного верстата</a>
      <a href="http://server.com/Calculate_Project/index_react/Step%2011.php">КРОК 11. Вираховування потужності електродвигуна  насосу охолодження свердлильного верстата</a>
      <a href="http://server.com/Calculate_Project/index_react/Step%2012.php">КРОК 12. Розрахунок електрообладнання свердлильного верстата</a>
      <a href="http://server.com/Calculate_Project/index_react/Combined.php">Обєднаний варіант</a>

    </div>
<div id="id_step_2" class="formula_2_step">
  <h1 align="center">Вираховування осьової складової сили різання, Р0, Н</h1>
  <a href="http://server.com/Calculate_Project/index_react/Step%202.1.php">КРОК 2.1. Здійснюємо перевірку виконання умови P0<Рmax </a>
  <a href="http://server.com/Calculate_Project/index_react/Step%202.2.php">КРОК 2.2. Здійснюємо вираховування подачі при перевантаженні механізму подачі верстата</a>

</div>


<div id="id_step_3" class="formula_3_step">
  <h1 align="center">Вираховування поправочного коефіцієнта, Kv, який враховує вплив механічних властивостей оброблювального матеріалу на швидкість різання, де оброблювальний матеріал сталь конструкційна вуглеродиста, легована і стальне лиття при матеріалі ріжучої частини інструмента - швидкоріжуча сталь</h1>
  <a href="http://server.com/Calculate_Project/index_react/Step%203.1.php">КРОК 3.1. Вираховування "першого" поправочного коефіцієнта Kmv </a>
  <a href="http://server.com/Calculate_Project/index_react/Step%203.2.php">Крок 3.2. Вираховування поправочних коефіцієнтів Kuv,Klv, які враховують відповідно марку інструментального матеріалу та вплив глибини оброблювального отвору</a>

</div>






      <script src="script.js"></script>
  </body>
</html>
