<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Крок 3.2</title>
  </head>
  <body>
    <form method="GET">
    <fieldset>
    <legend> Крок 3.2. Вираховування поправочних коефіцієнтів Kuv,Klv, які враховують відповідно марку інструментального матеріалу та вплив глибини оброблювального отвору </legend>
          <p>Поправочний коефіцієнт Kuv, що враховує марку інструментального матеріалу: <select type="text" name="Kuv" value="" size="1px"></p>
          <optgroup label="Оброблювальний матеріал">
          <option value="0.35">Cталь і конструкційне лиття Т5К12В, Kuv=0.35</option>
          <optgroup label="Оброблювальний матеріал">
          <option value="0.65">Cталь і конструкційне лиття Т5К10, Kuv=0.65</option>
          <optgroup label="Оброблювальний матеріал">
          <option value="0.8">Cталь і конструкційне лиття Т14К8, Kuv=0.8</option>
          <optgroup label="Оброблювальний матеріал">
          <option value="1.0">Cталь і конструкційне лиття Т15К6, Kuv=1.0</option>
          <optgroup label="Оброблювальний матеріал">
          <option value="1.15">Cталь і конструкційне лиття Т15К6, Kuv=1.15</option>
          <optgroup label="Оброблювальний матеріал">
          <option value="1.4">Cталь і конструкційне лиття Т30К4, Kuv=1.4</option>
          <optgroup label="Оброблювальний матеріал - ">
          <option value="0.4">Cталь і конструкційне лиття ВК8, Kuv=0.4</option>
          </optgroup>
          </select></p>

          <p>Поправочний коефіцієнт Klv, на швидкість різання, що враховує вплив глибини оброблювального отвору: <select  type="text" name="Klv" value="" size="1px"></p>
          <optgroup label="Глибина оброблювального отвору у діаметрах - 3D">
          <option value="1.0">Klv=1.0</option>
          <optgroup label="Глибина оброблювального отвору у діаметрах - 4D">
          <option value="0.85">Klv=0.85</option>
          <optgroup label="Глибина оброблювального отвору у діаметрах - 5D">
          <option value="0.75">Klv=0.75</option>
          <optgroup label="Глибина оброблювального отвору у діаметрах - 6D">
          <option value="0.7">Klv=0.7</option>
          <optgroup label="Глибина оброблювального отвору у діаметрах - 8D">
          <option value="0.6">Klv=0.6</option>
          <optgroup label="Глибина оброблювального отвору у діаметрах - більше 8D">
          <option value="1.0">Klv=1.0</option>
          </optgroup>
          </select></p>

          <p>Поправочний коефіцієнт Kmv, на швидкість головного руху різання: <input  type="text" name="Kmv" value="" size="1px"></p>
          </input></p>
          <input type="submit"  name="" value="Вирахувати поправочний коефіцієнт, Kv, на швидкість головного руху різання">
          <input type="text" name="" value="
    <?php

              $Klv = $_GET['Klv'];
              $Kuv = $_GET['Kuv'];
              $Kmv = $_GET['Kmv'];
              $Kv=$Klv*$Kuv*  $Kmv;
              echo $Kv;

             ?>
    ">
     </fieldset>
      </form>
  </body>
</html>
