<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>КРОК 3.1</title>
  </head>
  <body>
    <form method="GET">
      <fieldset>
       <legend> КРОК 3.1. Вираховування "першого" поправочного коефіцієнта Kmv </legend>
       <p>Коефіцієнт См, що враховує групу сталі: <select  type="text" name="Cm" value="" size="1px"></p>
       <optgroup label="Оброблювальний матеріал - cталь вуглеродиста, cталь нікелева, cталь хромиста">
       <option value="1.0">См=1.0</option>
       <optgroup label="Оброблювальний матеріал - cталь автоматна">
       <option value="1.2">См=1.2</option>
       <optgroup label="Оброблювальний матеріал - cталь хромонікелева">
       <option value="0.9">См=0.9</option>
       </optgroup>
       </select></p>

       <p>Показник степені у формулі вираховування поправочного коефіцієнта при операції свердління і розсвердлювання, nv: <select type="text" name="nv" value=""size="1px"></p>
       <optgroup label="Оброблювальний матеріал - cталь вуглеродиста, cталь автоматна, cталь нікелева, cталь хромиста, cталь хромонікелева">
       <option value="0.9">nv=0.9</option>
       </optgroup>
       </select></p>

       <p>Межа міцності матеріалу заготовки, сігма, МПа: <input type="text" name="sigb" value="" size="2px"></p>
       </p>
       <input type="submit" name="" value="Вирахувати поправочний коефіцієнт, Kmv, на швидкість головного руху різання">
       <input type="text" name="" value="
       <?php
         $sigb = $_GET['sigb'];
         $nv = $_GET['nv'];
         $Cm = $_GET['Cm'];
         $Kmv=  $Cm*( pow(75/$sigb, $nv));
                   echo $Kmv;

        ?>
        ">
</fieldset>
 </form>

  </body>
</html>
