<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>КРОК 5</title>
  </head>
  <body>
    <form method="GET">
    <!-- Створюємо блок групування елементів у формі для вираховування частоти обертання шпинделя-->
    <fieldset>
    <legend>КРОК 5. Вираховування частоти обертання шпинделя, що відповідає знайденій швидкості головного руху різання n, хв</legend>
    <p>Діаметр ріжучого інструменту D, мм:</p><input  type="text" name="D" value="" size="1px">
    <p> Швидкість головного руху різання, що допускається ріжучими властивостями свердла v, м/хв:</p><input  type="text" name="v" value="" size="1px">
    </input></p>
    <input type="submit" name="" value="Вирахувати частоту обертання шпинделя, що відповідає знайденій швидкості головного руху різання n, хв">
    <input type="text" name="" value="
    <?php
    $D = $_GET['D'];
    $v = $_GET['v'];
    $n=1000*$v/(3.14*$D);
    echo round ($n, 3);
    ?>
    ">
    </form>
    </fieldset>
  </body>
</html>
