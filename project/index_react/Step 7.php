<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>КРОК 7</title>
  </head>
  <body>
    <form method="GET">
    <!-- Створюємо блок групування елементів у формі для вираховування крутного моменту від сил опору різання при свердлінні-->
    <fieldset>
    <legend> КРОК 7. Вираховування крутного моменту від сил опору різання при свердлінні М, Нм</legend>
    <p>Коефіцієнт формули крутного моменту, що залежить від властивостей матеріалу, який обробляється, Cм:</p>
    <select  type="text" name="Cm" value="" size="1px">
    <optgroup label="Обробка матеріалу сталь конструкційна і стальне лиття з межою міцності 750 Мпа шляхлом свердління, де матеріал ріжучої  частини інструмента - швидкоріжуча сталь">
    <option value="0.0345">0.0345</option>
    <optgroup label="Обробка матеріалу сталь конструкційна і стальне лиття з межою міцності 750 Мпа шляхлом розсвердлювання, де матеріал ріжучої  частини інструмента - швидкоріжуча сталь">
    <option value="0.09">0.09</option>
    <optgroup label="Обробка матеріалу сталь жаростійка Х18Н9Т шляхлом свердління, де матеріал ріжучої  частини інструмента - швидкоріжуча сталь">
    <option value="0.041">0.041</option>
    </optgroup>
    </select></p>

    <p>Показник степені формули крутного моменту, що залежить від властивостей матеріалу, який обробляється, qм:</p>
    <select  type="text" name="qm" value="" size="1px">
    <optgroup label="Обробка матеріалу сталь конструкційна і стальне лиття з межою міцності 750 Мпа шляхлом свердління, де матеріал ріжучої  частини інструмента - швидкоріжуча сталь">
    <option value="2.0">2.0</option>
    <optgroup label="Обробка матеріалу сталь конструкційна і стальне лиття з межою міцності 750 Мпа шляхлом розсвердлювання, де матеріал ріжучої  частини інструмента - швидкоріжуча сталь">
    <option value="1.0">1.0</option>
    <optgroup label="Обробка матеріалу сталь жаростійка Х18Н9Т шляхлом свердління, де матеріал ріжучої  частини інструмента - швидкоріжуча сталь">
    <option value="2.0">2.0</option>
    </optgroup>
    </select></p>

    <p>Показник степені формули о крутного моменту, що залежить від властивостей матеріалу, який обробляється, yм:</p>
    <select  type="text" name="ym" value="" size="1px">
    <optgroup label="Обробка матеріалу сталь конструкційна і стальне лиття з межою міцності 750 Мпа шляхлом свердління, де матеріал ріжучої  частини інструмента - швидкоріжуча сталь">
    <option value="0.8">0.8</option>
    <optgroup label="Обробка матеріалу сталь конструкційна і стальне лиття з межою міцності 750 Мпа шляхлом розсвердлювання, де матеріал ріжучої  частини інструмента - швидкоріжуча сталь">
    <option value="0.8">0.8</option>
    <optgroup label="Обробка матеріалу сталь жаростійка Х18Н9Т шляхлом свердління, де матеріал ріжучої  частини інструмента - швидкоріжуча сталь">
    <option value="0.7">0.7</option>
    </optgroup>
    </select></p>

    <p>Діаметр ріжучого інструменту D, мм:</p><input  type="text" name="D" value="" size="1px">
    <p>Подача S0, мм/об:</p><input  type="text" name="S0" value="" size="1px">
    <p>Поправочний коефіцієнт на осьову складову сили різання, Kp:</p><input  type="text" name="Kp" value="" size="1px">
    </p>
    <input type="submit" name="" value="Вирахувати крутний момент від сил опору різання при свердлінні М, Нм">
    <input type="text" name="" value="
    <?php
    $Cm = $_GET['Cm'];
    $D = $_GET['D'];
    $qm = $_GET['qm'];
    $S0 = $_GET['S0'];
    $ym = $_GET['ym'];
    $Kp = $_GET['Kp'];
    $M=$_GET["M"];
    $M1=$_GET["M1"];
    $M=9.81 * $Cm * (pow($D,$qm));
    $M1=(pow($S0,$ym));
    echo ($M*$M1) * $Kp * 0.101973;
    ?>
    ">
    </form>
    </fieldset>

  </body>
</html>
